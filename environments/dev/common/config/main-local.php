<?php
return [
    'name' => 'Mods',   // Название сайта
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=yii2advanced',    // localhost - Хост, yii2advanced - имя БД
            'username' => 'root',   // логин
            'password' => '',   // пароль
            'charset' => 'utf8',
            'enableSchemaCache' => true,
        ],
    ],
];
