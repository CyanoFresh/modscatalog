<?php
return [
    'title' => [
        'connector' => ' - ',   // Разделитель в title
        'default' => 'Mods',    // Title по умолчанию
    ],
    'home' => [
        'title' => 'Главная',   // title главной
        'description' => 'Описание главной',   // description главной
        'keywords' => 'Ключ слова',   // keywords главной
    ],
];
