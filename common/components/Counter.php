<?php

namespace common\components;

use Yii;
use yii\base\Object;
use yii\web\Cookie;

class Counter extends Object
{
    public function plus($model)
    {
        $name = md5($model->name);
        $id = md5($model->id);

        $session = Yii::$app->session;
        $cookies = Yii::$app->request->cookies;

        if (!$session->has($name) and $cookies->has($name)) {
            $session->set($name, $id);
        }

        if (!$cookies->has($name) and $session->has($name)) {
            Yii::$app->response->cookies->add(new Cookie([
                'name' => $name,
                'value' => $id,
                'expire' => time() + 31556926,
            ]));
        }

        if (!$session->has($name) and !$cookies->has($name)) {
            $model->views = $model->views + 1;

            $session->set($name, $id);

            Yii::$app->response->cookies->add(new Cookie([
                'name' => $name,
                'value' => $id,
                'expire' => time() + 31556926,
            ]));

            $model->save();
        }
    }
}