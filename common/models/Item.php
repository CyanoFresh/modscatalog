<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\StringHelper;
use Zelenin\yii\behaviors\Slug;

/**
 * This is the model class for table "item".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $views
 * @property integer $downloads
 * @property integer $likes
 * @property integer $dislikes
 * @property string $filename
 * @property string $video
 * @property string $date
 * @property string $author
 * @property string $slug
 * @property string $name
 * @property string $short
 * @property string $description
 * @property string $meta_description
 * @property string $meta_keywords
 *
 * @property Image[] $images
 * @property Category $category
 */
class Item extends ActiveRecord
{
    /**
     * @var \yii\web\UploadedFile files attribute
     */
    public $image;
    /**
     * @var \yii\web\UploadedFile file attribute
     */
    public $file;
    /**
     * @var \yii\web\UploadedFile files attribute
     */
    public $images;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'author', 'name', 'description', 'short'], 'required'],
            [['category_id', 'views', 'downloads', 'likes', 'dislikes'], 'integer'],
            [['date'], 'safe'],
            [['short', 'description'], 'string'],
            [['filename', 'video', 'author', 'slug', 'name', 'meta_description', 'meta_keywords'], 'string', 'max' => 255],
            [['image', 'images', 'file'], 'required', 'on' => 'create'],
            [['image'], 'image', 'mimeTypes' => 'image/jpeg, image/png'],
            [['file'], 'file'],
            [['images'], 'image', 'maxFiles' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Превью',
            'images' => 'Скриншоты',
            'file' => 'Файл',
            'filename' => 'Имя Файла',
            'video' => 'Видео',
            'category_id' => 'Категория',
            'views' => 'Просмотров',
            'downloads' => 'Загрузок',
            'likes' => 'Лайков',
            'dislikes' => 'Дизлайков',
            'date' => 'Дата',
            'author' => 'Автор',
            'slug' => 'URL',
            'name' => 'Название',
            'short' => 'Краткое Описание',
            'description' => 'Описание',
            'meta_description' => 'Meta Описание',
            'meta_keywords' => 'Meta Ключевые слова',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => Slug::className(),
                'attribute' => ['name'],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return string
     */
    public function getImageUrlFrontEnd()
    {
        return Yii::$app->urlManager->baseUrl . '/uploads/item/' . $this->id . '/main.jpg';
    }

    /**
     * @return string
     */
    public function getImageUrlBackEnd()
    {
        return Yii::$app->urlManagerFrontEnd->baseUrl . '/uploads/item/' . $this->id . '/main.jpg';
    }

    public function getUrl()
    {
        return [
            'file/view',
            'item' => $this->slug,
            'category' => $this->category->slug,
        ];
    }

    public function getTitle()
    {
        $title = $this->name;
        Yii::$app->params['title_cut'];
        Yii::$app->params['title_after'];

        $title = StringHelper::truncate($title, Yii::$app->params['title_cut'], Yii::$app->params['title_after']);

        return $title;
    }
}
