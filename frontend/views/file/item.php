<?php

use common\models\Image;
use common\components\VideoEmbed;
use dosamigos\gallery\Gallery;
use kartik\icons\Icon;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Item */

$category = $model->category;

$this->title = $model->name . Yii::$app->params['title']['connector'] . $category->name;

if ($category->parent) {
    $this->title .= Yii::$app->params['title']['connector'] . $category->parent->name;
    $this->params['breadcrumbs'][] = ['label' => $category->parent->name, 'url' => ['category', 'category' => $category->parent->slug]];
}
$this->params['breadcrumbs'][] = ['label' => $category->name, 'url' => ['category', 'category' => $category->slug]];
$this->params['breadcrumbs'][] = $model->name;

// Meta tags
$this->registerMetaTag(['name' => 'description', 'content' => $model->meta_description]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $model->meta_keywords]);

// Screens
$screens[] = [
    'src' => Yii::$app->urlManager->baseUrl . '/uploads/item/' . $model->id . '/main.jpg',
    'options' => [
        'title' => $model->name,
        'class' => 'thumbnail',
    ],
];
foreach (Image::findAll(['item_id' => $model->id]) as $image) {
    $screens[] = [
        'src' => Yii::$app->urlManager->baseUrl . '/uploads/item/' . $model->id . '/' . $image->id . '.jpg',
        'options' => [
            'title' => $model->name,
            'class' => 'thumbnail',
        ],
    ];
}
?>

<div class="file-view">
    <h1><?= $model->name ?></h1>
    <div class="row">
        <div class="col-sm-5">
            <?= Html::a(
                'Скачать',
                ['download', 'item' => $model->slug],
                [
                    'class' => 'btn btn-primary btn-lg btn-block btn-download'
                ]
            ) ?>

            <div class="row" style="margin-bottom: 23px">
                <div class="col-xs-6">
                    <?= Html::a(Icon::show('thumbs-up') . 'Нравится', ['rate', 'item' => $model->slug, 'up' => '1'], [
                        'class' => 'btn btn-success btn-block'
                    ]) ?>
                </div>
                <div class="col-xs-6">
                    <?= Html::a(Icon::show('thumbs-down') . 'Не Нравится', ['rate', 'item' => $model->slug, 'up' => '2'], [
                        'class' => 'btn btn-danger btn-block'
                    ]) ?>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <?= Icon::show('user') . $model->author ?> ·
                    <?= Icon::show('calendar') . $model->date ?>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $model->description ?>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <?= Icon::show('book') . Html::a(
                        $model->category->name,
                        ['file/category', 'category' => $model->category->slug],
                        ['class' => 'category-link']
                    ) ?> ·
                    <?= Icon::show('download') . $model->downloads ?> ·
                    <?= Icon::show('eye') . $model->views ?> ·
                    <?= Icon::show('thumbs-up') . $model->likes ?> ·
                    <?= Icon::show('thumbs-down') . $model->dislikes ?>
                </div>
            </div>
        </div>
        <div class="col-sm-7">
            <div class="screens">
                <?= Gallery::widget([
                    'items' => $screens,
                    'id' => 'screens',
                ]) ?>
            </div>
            <?php if ($model->video): ?>
                <hr>
                <div class="video">
                    <?= VideoEmbed::widget(['url' => $model->video]); ?>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>