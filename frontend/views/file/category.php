<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $dataProvider yii\data\ActiveDataProvider */

// Meta tags
$this->registerMetaTag(['name' => 'description', 'content' => $model->meta_description]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $model->meta_keywords]);
$this->title = $model->name;

// Breadcrumbs
if ($model->parent_id) {
    $this->title .= Yii::$app->params['title']['connector'] . $model->parent->name;
    $this->params['breadcrumbs'][] = ['label' => $model->parent->name, 'url' => ['category', 'category' => $model->parent->slug]];
}

$this->params['breadcrumbs'][] = $model->name;
?>

<h1 class="page-header"><?= $model->name ?></h1>

<div class="row">
    <?php foreach ($model->categories as $category): ?>
        <div class="col-sm-4">
            <div class="panel panel-default panel-subcategory">
                <div class="panel-body text-center">
                    <h3><?= Html::a($category->name, ['category', 'category' => $category->slug]) ?></h3>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>

<div class="well">
    Сортировать по:

    <?= $dataProvider->sort->link('name', [
        'label' => 'Имени',
        'class' => 'btn btn-primary btn-sm',
    ]) ?>
    <?= $dataProvider->sort->link('downloads', [
        'label' => 'Загрузкам',
        'class' => 'btn btn-primary btn-sm'
    ]) ?>
    <?= $dataProvider->sort->link('views', [
        'label' => 'Просмотрам',
        'class' => 'btn btn-primary btn-sm'
    ]) ?>
    <?= $dataProvider->sort->link('likes', [
        'label' => 'Лайкам',
        'class' => 'btn btn-primary btn-sm'
    ]) ?>
    <?= $dataProvider->sort->link('dislikes', [
        'label' => 'Дизлайкам',
        'class' => 'btn btn-primary btn-sm'
    ]) ?>
    <?= $dataProvider->sort->link('date', [
        'label' => 'Дате',
        'class' => 'btn btn-primary btn-sm'
    ]) ?>
</div>

<?= ListView::widget([
    'layout' => "<div class=\"row\">{items}</div>\n{pager}",
    'dataProvider' => $dataProvider,
    'itemView' => '_item',
    'summaryOptions' => [
        'class' => 'alert alert-info'
    ],
]) ?>