<?php
use kartik\icons\Icon;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Item */
?>

<div class="col-sm-4">
    <div class="panel panel-default">
        <div class="panel-image">
            <?= Html::a(
                Html::img($model->getImageUrlFrontEnd(), [
                    'class' => 'img-responsive',
                ]),
                $model->url,
                ['class' => 'click-link']
            ) ?>
        </div>
        <div class="panel-body">
            <h4>
                <?= Html::a(
                    $model->title,
                    $model->url,
                    ['class' => 'click-link']
                ) ?>
            </h4>

            <div class="short-description">
                <?= $model->short ?>
            </div>

            <p class="text-muted">
                <?= Icon::show('book') . Html::a(
                    $model->category->name,
                    ['file/category', 'category' => $model->category->slug],
                    ['class' => 'category-link']
                ) ?> ·
                <?= Icon::show('eye') . $model->views ?> ·
                <?= Icon::show('thumbs-up') . $model->likes ?> ·
                <?= Icon::show('thumbs-down') . $model->dislikes ?>
            </p>

            <?= Html::a('Скачать', $model->url, [
                'class' => 'btn btn-primary',
            ]) ?>
        </div>
    </div>
</div>