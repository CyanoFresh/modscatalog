<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<div class="site-error">

    <h1><?= $this->title ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        Данная ошибка произошла при обработке Вашего запроса сервером
    </p>
    <p>
        Свяжитесь с нами, если Вы считаете, что это наша ошибка
    </p>

</div>
