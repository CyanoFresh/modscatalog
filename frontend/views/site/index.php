<?php
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

// Meta tags
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['home']['description']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['home']['keywords']]);
$this->title = Yii::$app->params['home']['title'];
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Добро пожаловать!</h1>

        <p class="lead">
            Вы находитесь в каталоге модов. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean arcu mi, bibendum ut sollicitudin vel, commodo sed ante. In placerat libero et metus consectetur laoreet.
        </p>

        <p><a class="btn btn-primary btn-lg" href="" role="button">Кнопка</a></p>
    </div>

    <h1 class="page-header text-center">
        Последние файлы
    </h1>

    <?= ListView::widget([
        'layout' => "<div class=\"row\">{items}</div>",
        'dataProvider' => $dataProvider,
        'itemView' => '_item',
        'summaryOptions' => [
            'class' => 'alert alert-info'
        ],
    ]) ?>
</div>
