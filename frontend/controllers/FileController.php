<?php

namespace frontend\controllers;

use common\components\Counter;
use common\models\Category;
use common\models\Item;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\NotFoundHttpException;

class FileController extends Controller
{
    public function actionCategory($category)
    {
        $model = $this->findCategory($category);

        $ids = [$model->id];

        foreach (Category::findAll(['parent_id' => $model->id]) as $category) {
            $ids[] = $category->id;
        }

        $query = Item::find()->where([
            'category_id' => $ids
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                // Order by date for default
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ]
            ],
            'pagination' => [
                // Prevent creating additional params in the URL. For SEO
                'forcePageParam' => false,
                'pageSizeParam' => false,
            ],
        ]);

        return $this->render('category', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($item)
    {
        $model = $this->findModel($item);

        Yii::$app->counter->plus($model);

        return $this->render('item', [
            'model' => $model,
        ]);
    }

    public function actionDownload($item)
    {
        $model = $this->findModel($item);

        $model->downloads++;
        $model->save();

        $file = Yii::getAlias('@frontend/web/uploads/item/' . $model->id . '/' . $model->filename);

        return Yii::$app->response->sendFile($file, $model->filename);
    }

    public function actionRate($item, $up)
    {
        $model = $this->findModel($item);

        $name = md5($model->name . 'views');

        $session = Yii::$app->session;
        $cookies = Yii::$app->request->cookies;

        if (!$session->has($name) and $cookies->has($name)) {
            $session->set($name, $model->id);
        }

        if (!$cookies->has($name) and $session->has($name)) {
            Yii::$app->response->cookies->add(new Cookie([
                'name' => $name,
                'value' => $model->id,
                'expire' => time() + 31556926,
            ]));
        }

        if ($session->has($name) and $cookies->has($name)) {
            $session->setFlash('danger', 'Ваш голос уже был учтен ранее.');
        } else {
            if ($up === '1') {
                $model->likes++;
            } elseif ($up === '2') {
                $model->dislikes++;
            }

            $session->set($name, $model->id);

            Yii::$app->response->cookies->add(new Cookie([
                'name' => $name,
                'value' => $model->id,
                'expire' => time() + 31556926,
            ]));

            $model->save();

            $session->setFlash('success', 'Ваш голос был засчитан');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Item model based on its slug.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        if (($model = Item::findOne(['slug' => $slug])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Файл не найден');
        }
    }

    /**
     * Finds the Category model based on its slug.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCategory($slug)
    {
        if (($model = Category::findOne(['slug' => $slug])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Категория не найдена');
        }
    }

}
