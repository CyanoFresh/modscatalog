<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use common\models\Item;
use common\models\Category;
use yii\web\Response;

class SitemapController extends Controller
{
    public $urls = [];

    public function actionIndex()
    {
        $this->urls = $this->getUrls();

        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml');

        $sitemap = $this->renderPartial('index', ['urls' => $this->urls]);

        return $sitemap;
    }

    public function getUrls()
    {
        $urls = [];

        $urls[] = Url::to(['site/index'], true);

        foreach (Category::find()->all() as $item) {
            $urls[] = Url::to(['file/category', 'category' => $item->slug], true);
        }
        foreach (Item::find()->all() as $item) {
            $urls[] = Url::to($item->url, true);
        }

        return $urls;
    }
}