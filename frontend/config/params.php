<?php
return [
    'adminEmail' => 'admin@example.com',
    'title' => [
        'connector' => ' - ',
        'default' => 'Mods',
    ],
    'home' => [
        'title' => 'Главная',
        'description' => 'Описание главной',
        'keywords' => 'Ключ слова',
    ],
    'title_cut' => 20,
    'title_after' => '...',
];
