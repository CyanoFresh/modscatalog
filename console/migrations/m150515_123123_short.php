<?php

use yii\db\Schema;
use yii\db\Migration;

class m150515_123123_short extends Migration
{
    public function up()
    {
        $this->addColumn('{{%item}}', 'short', 'TEXT NULL AFTER `name`');
    }

    public function down()
    {
        $this->dropColumn('{{%item}}', 'short');
    }
}
