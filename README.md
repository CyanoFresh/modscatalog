Mods Catalog
===================================

Mods Catalog for GTA 5

INSTALLATION
------------

1 .Run in the console (`mods` - project catalog): 

~~~
git clone https://github.com/CyanoFresh/modscatalog.git mods
cd mods
php init
~~~

2. Configure database in the file common/config/main-local.php

3. Run in the console:

~~~
php yii migrate
~~~

4. Set document roots of your Web server:

- for frontend `/path/to/mods/frontend/web/` and using the URL `http://frontend/`
- for backend `/path/to/mods/backend/web/` and using the URL `http://backend/`

5. Set `baseUrl` in the backend/config/main-local.php that you provided in step 4

6. Configure app by setting `environments\prod\common\config\main-local.php` and `common\config\params-local.php`