<?php

namespace backend\controllers;

use Yii;
use common\models\Item;
use common\models\ItemSearch;
use common\models\Image;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update' , 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Item model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Item model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Item(['scenario' => 'create']);

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->file = UploadedFile::getInstance($model, 'file');
            $model->images = UploadedFile::getInstances($model, 'images');
            $filename = $model->file->name;
            $model->filename = $filename;

            if ($model->validate() && $model->save()) {
                $dir = Yii::getAlias('@frontend/web/uploads/item/' . $model->id . '/');
                FileHelper::createDirectory($dir);

                // Save main image
                $model->image->saveAs($dir . 'main.jpg');

                // Save file
                $model->file->saveAs($dir . $filename);

                // Save images
                if ($model->images) {
                    foreach ($model->images as $image) {
                        $imageModel = new Image();
                        $imageModel->item_id = $model->id;
                        $imageModel->save();

                        $image->saveAs($dir . $imageModel->id . '.jpg');
                    }
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        $model->date = date('Y-m-d H:i');
        $model->likes = 0;
        $model->dislikes = 0;
        $model->downloads = 0;
        $model->author = Yii::$app->user->identity->username;

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Item model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->file = UploadedFile::getInstance($model, 'file');
            $model->images = UploadedFile::getInstances($model, 'images');

            if ($model->validate() && $model->save()) {
                $dir = Yii::getAlias('@frontend/web/uploads/item/' . $model->id . '/');

                // Save main image
                if ($model->image) {
                    $model->image->saveAs($dir . 'main.jpg');
                }

                // Save file
                if ($model->file) {
                    $filename = $model->file->name;
                    $model->filename = $filename;
                    $model->save();
                    $model->file->saveAs($dir . $filename);
                }

                // Save images
                if ($model->images) {
                    $imageModels = Image::findAll(['item_id' => $model->id]);
                    if ($imageModels) {
                        foreach ($imageModels as $image) {
                            $file = $dir . $image->id . '.jpg';

                            if (file_exists($file)) {
                                unlink($file);
                            }

                            $image->delete();
                        }
                    }

                    foreach ($model->images as $image) {
                        $imageModel = new Image();
                        $imageModel->item_id = $model->id;
                        $imageModel->save();

                        $image->saveAs($dir . $imageModel->id . '.jpg');
                    }
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Item model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $dir = Yii::getAlias('@frontend/web/uploads/item/' . $model->id);
        FileHelper::removeDirectory($dir);

        Image::deleteAll(['item_id' => $model->id]);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
