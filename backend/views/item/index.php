<?php
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Файлы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-index">

    <h1 class="page-header">
        <?= $this->title ?>
        <?= Html::a(Icon::show('plus'), ['create'], ['class' => 'btn btn-success pull-right']) ?>
    </h1>

    <?= $this->render('_search', [
        'model' => $searchModel
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summaryOptions' => ['class' => 'alert alert-info'],
        'columns' => [
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function ($model) {
                    /** @var $model common\models\Item */
                    return Html::img($model->imageUrlBackEnd, ['width' => 100]);
                },
            ],
            'name',
            [
                'attribute' => 'category_id',
                'format' => 'html',
                'value' => function ($model) {
                    /** @var $model common\models\Item */
                    return Html::a($model->category->name, ['category/view', 'id' => $model->category->id]);
                },
            ],
            'date',
            'slug',
            // 'downloads',
            // 'likes',
            // 'dislikes',
            // 'author',
            // 'description:ntext',
            // 'meta_description',
            // 'meta_keywords',

            ['class' => 'common\components\ActionButtonGroupColumn'],
        ],
    ]); ?>

</div>
