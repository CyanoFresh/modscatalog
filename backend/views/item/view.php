<?php
use common\models\Image;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Item */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Файлы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

// Get file images
$imagesModel = Image::find()->where(['item_id' => $model->id])->all();
if ($imagesModel) {
    $images = null;

    foreach ($imagesModel as $imageModel) {
        /** @var $imageModel common\models\Image */
        $images .= Html::img(
            Yii::$app->urlManagerFrontEnd->baseUrl . '/uploads/item/' . $model->id . '/' . $imageModel->id . '.jpg',
            [
                'width' => '200px',
                'class' => 'img-thumbnail'
            ]
        );
    };
} else {
    $images = false;
}
?>
<div class="item-view">

    <h1 class="page-header">
        <?= $this->title ?>
        <div class="btn-group pull-right">
            <?= Html::a(
                Icon::show('eye'),
                Yii::$app->urlManagerFrontEnd->createUrl(['file/view', 'item' => $model->slug, 'category' => $model->category->slug]),
                [
                    'class' => 'btn btn-success',
                    'target' => '_blank',
                    'title' => 'На сайте',
                ]
            ) ?>
            <?= Html::a(
                Icon::show('pencil'),
                ['update', 'id' => $model->id],
                [
                    'class' => 'btn btn-primary',
                    'title' => 'Редактировать',
                ]
            ) ?>
            <?= Html::a(
                Icon::show('trash'),
                ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'title' => 'Удалить',
                    'data' => [
                        'confirm' => 'Вы действительно хотите удалить этот файл?',
                        'method' => 'post',
                    ]
                ]
            ) ?>
        </div>
    </h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => Html::img($model->imageUrlBackend, [
                    'class' => 'img-thumbnail',
                    'width' => 200,
                ]),
            ],
            [
                'attribute' => 'images',
                'format' => 'html',
                'value' => $images,
            ],
            [
                'attribute' => 'file',
                'format' => 'html',
                'value' => Html::a(
                    $model->filename,
                    Yii::$app->urlManagerFrontEnd->createUrl(['file/download', 'item' => $model->slug])
                ),
            ],
            [
                'attribute' => 'category_id',
                'format' => 'html',
                'value' => Html::a($model->category->name, ['category/view', 'id' => $model->category->id]),
            ],
            'downloads',
            'likes',
            'dislikes',
            'date',
            'author',
            'slug',
            'name',
            'short:html',
            'description:html',
            'meta_description',
            'meta_keywords',
        ],
    ]) ?>

</div>
