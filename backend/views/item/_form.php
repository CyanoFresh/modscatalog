<?php

use common\models\Category;
use common\models\Image;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget as Imperavi;
use kartik\widgets\DateTimePicker;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Item */
/* @var $form yii\widgets\ActiveForm */

// Get category list
$category_models = Category::find()->all();
$categories = ArrayHelper::map($category_models, 'id', 'name');

// Get main image
$image = $model->isNewRecord ? false : Html::img(
    $model->imageUrlBackEnd,
    ['class' => 'file-preview-image']
);

// Get file images
$imagesModel = Image::find()->where(['item_id' => $model->id])->all();
if ($imagesModel) {
    $images = [];

    foreach ($imagesModel as $imageModel) {
        /** @var $imageModel common\models\Image */
        $images[] = Html::img(
            Yii::$app->urlManagerFrontEnd->baseUrl . '/uploads/item/' . $model->id . '/' . $imageModel->id . '.jpg',
            ['class' => 'file-preview-image']
        );
    };
} else {
    $images = false;
}

// Get uploaded file
$file = $model->isNewRecord ? false : "<div class='file-preview-text'>"
    . "<h2><i class='glyphicon glyphicon-file'></i></h2>"
    . $model->filename
    . "</div>";
?>

<div class="item-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'image')->widget(FileInput::className(), [
        'options' => [
            'accept' => 'image/*',
        ],
        'pluginOptions' => [
            'showUpload' => false,
            'browseLabel' => 'Выбрать',
            'removeLabel' => 'Удалить',
            'removeClass' => 'btn btn-danger',
            'initialPreview' => $image,
        ],
    ]) ?>

    <?= $form->field($model, 'images[]')->widget(FileInput::className(), [
        'options' => [
            'accept' => 'image/*',
            'multiple' => true,
        ],
        'pluginOptions' => [
            'maxFileCount' => 10,
            'showUpload' => false,
            'browseLabel' => 'Выбрать',
            'removeLabel' => 'Очистить',
            'removeClass' => 'btn btn-danger',
            'initialPreview' => $images,
        ],
    ]) ?>

    <?= $form->field($model, 'file')->widget(FileInput::className(), [
        'pluginOptions' => [
            'showUpload' => false,
            'browseLabel' => 'Выбрать',
            'removeLabel' => 'Очистить',
            'removeClass' => 'btn btn-danger',
            'showPreview' => false,
            'initialPreview' => $file,
        ],
    ]) ?>

    <?= $form->field($model, 'video')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'short')->widget(Imperavi::className(), [
        'settings' => [
            'minHeight' => 100,
        ],
    ]) ?>

    <?= $form->field($model, 'description')->widget(Imperavi::className(), [
        'settings' => [
            'minHeight' => 200,
        ],
    ]) ?>

    <?= $form->field($model, 'category_id')->dropDownList($categories, [
        'prompt' => '--- выберите категорию ---',
    ]) ?>

    <?= $form->field($model, 'date')->widget(DateTimePicker::className(), [
        'pluginOptions' => [
            'autoclose' => true,
            'todayHighlight' => true,
            'format' => 'yyyy-mm-dd hh:ii',
            'todayBtn' => true,
            'convertFormat' => true,
        ],
    ]) ?>

    <?= $form->field($model, 'author')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'slug')
        ->textInput(['maxlength' => 255])
        ->hint('Если пусто - будет сгенерировано с имени') ?>

    <?= $form->field($model, 'meta_description')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Добавить' : 'Сохранить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
