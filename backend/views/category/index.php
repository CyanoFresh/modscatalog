<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1 class="page-header">
        <?= $this->title ?>
        <?= Html::a(Icon::show('plus'), ['create'], ['class' => 'btn btn-success pull-right']) ?>
    </h1>

    <?= $this->render('_search', [
        'model' => $searchModel
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summaryOptions' => ['class' => 'alert alert-info'],
        'columns' => [
            'id',
            'name',
            [
                'attribute' => 'parent_id',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->parent ? Html::a($model->parent->name, ['category/view', 'id' => $model->parent->id]) : null;
                },
            ],
            'slug',

            ['class' => 'common\components\ActionButtonGroupColumn'],
        ],
    ]); ?>

</div>
