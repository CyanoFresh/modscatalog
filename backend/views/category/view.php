<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-view">

    <h1 class="page-header">
        <?= $this->title ?>
        <div class="btn-group pull-right">
            <?= Html::a(
                Icon::show('eye'),
                Yii::$app->urlManagerFrontEnd->createUrl(['file/category', 'category' => $model->slug]),
                [
                    'class' => 'btn btn-success',
                    'target' => '_blank',
                    'title' => 'На сайте',
                ]
            ) ?>
            <?= Html::a(
                Icon::show('pencil'),
                ['update', 'id' => $model->id],
                [
                    'class' => 'btn btn-primary',
                    'title' => 'Редактировать',
                ]
            ) ?>
            <?= Html::a(
                Icon::show('trash'),
                ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'title' => 'Удалить',
                    'data' => [
                        'confirm' => 'Вы действительно хотите удалить эту категорию?',
                        'method' => 'post',
                    ]
                ]
            ) ?>
        </div>
    </h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'parent_id',
                'format' => 'html',
                'value' => $model->parent ? Html::a($model->parent->name, ['category/view', 'id' => $model->parent->id]) : null,
            ],
            'slug',
            'name',
            'meta_description',
            'meta_keywords',
        ],
    ]) ?>

</div>
